import { Action } from './types/types';
import { Component, OnInit, HostListener } from '@angular/core';
import { CoreService } from './core/core.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // Contract
  buttonAddress = '0x693e7a8f8DCc0206A4F9CDAefD0d09e0a510051f';
  Actions = Action;
  action = Action.Play;
  account: string;
  error = '';
  winner = '';
  button: any;
  age = 0;
  countDown = 0;
  waitTime = 0;
  balanceRefreshing = false;
  balance = 0;
  cost = 0;
  expired = false;
  waiting = true;

  // Instructions
  lastParticipant = '';
  instructionsExpanded = false;
  isMoving = false;

  // Internationalization
  selectedLang = '';
  data: any;
  main: any;
  langs = [];
  lang: string;
  arrLangs = [];
  receipt: any;

  constructor(private coreService: CoreService) {}

  ngOnInit() {
    // Initiol Setup
    this.setUpGame();
    // Refreshing data for the user to receive information in "real time"
    setInterval(() => this.getAge(), 1000);
    setInterval(() => this.isExpired(), 1000);
    setInterval(() => this.getLastParticipant(), 1000);

    // Loading data according to the language selected
    this.coreService.loadData();
    this.coreService.isDataLoaded.subscribe(() => {
      this.load();
    });
  }

  async setUpGame() {
    return await this.getAccount()
      .then(() => this.getButton())
      .then(() => this.isExpired())
      .then(() => this.getCost())
      .then(() => this.getLastBlock())
      .then(() => this.getWaitTime())
      .then(() => this.getAge())
      .then(() => this.getBalance())
      .then(() => {
        this.waiting = false;
      });
  }

  load() {
    const data = JSON.parse(localStorage.getItem('data'));
    const langs = JSON.parse(localStorage.getItem('langs'));
    const lang = localStorage.getItem('lang');

    if (!langs) {
      return;
    }

    const arrLangs = Object.keys(langs).map(key => {
      if (lang === key) {
        this.selectedLang = langs[key].description;
      }
      return langs[key];
    });

    setTimeout(() => {
      this.arrLangs = arrLangs;
      this.data = data;

      this.langs = langs;
      this.lang = lang;
    }, 0);
  }

  press() {
    this.waiting = true;
    if (this.action === Action.Claim) {
      this.claim();
    } else {
      this.isExpired()
        .then(() => this.getCost())
        .then(() => this.play());
    }
  }

  play() {
    if (this.expired || (this.age > this.waitTime && this.age < 100000)) {
      return;
    }
    this.coreService
      .press(this.button, this.account, this.cost)
      .on('confirmation', (confirmationNumber, receipt) => {
        console.log(confirmationNumber);

        if (confirmationNumber === 4) {
          this.refreshGame();
        }
      })
      .on('error', error => {
        this.refreshGame();
        this.reset();
        this.onError(error);
        return false;
      });
  }

  claim() {
    this.coreService
      .claim(this.button, this.account)
      .on('confirmation', (confirmationNumber, receipt) => {
        if (confirmationNumber === 4) {
          this.refreshGame();
        }
      })
      .on('error', error => {
        this.refreshGame();
        this.reset();
        this.onError(error);
        return false;
      });
  }

  async refreshGame() {
    return await this.getAccount()
      .then(() => this.isExpired())
      .then(() => this.getAge())
      .then(() => this.getBalance())
      .then(() => {
        if (!this.expired && this.age > 3000000) {
          this.action = Action.Play;
        }
        this.waiting = false;
      });
  }

  isReadyToClaim() {
    if (this.expired || (this.age > this.waitTime && this.age < 100000)) {
      this.getLastParticipant().then(() => {
        if (this.lastParticipant !== this.account) {
          this.action = Action.Play;
        } else {
          this.action = Action.Claim;
        }
      });
    } else {
      this.action = Action.Play;
    }
  }

  async getLastParticipant() {
    return await this.coreService
      .lastParticipant(this.button)
      .then(p => {
        this.lastParticipant = p;
        if (p === this.account) {
          this.winner = p;
        } else {
          this.winner = '';
        }
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getButton() {
    return await this.coreService
      .getButton()
      .then(button => {
        this.button = button;
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getAccount() {
    return await this.coreService
      .getAccount()
      .then(v => {
        this.account = v;
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getAge() {
    return await this.coreService
      .age(this.button)
      .then(a => {
        this.age = a;
        const count = this.waitTime - this.age + 1;
        this.countDown = count < 0 ? 0 : count;
        this.isReadyToClaim();
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getLastBlock() {
    return await this.coreService
      .lastBlock(this.button)
      .then(a => {
        this.age = a;
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getWaitTime() {
    return await this.coreService
      .waitTime(this.button)
      .then(w => {
        this.waitTime = w;
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async isExpired() {
    return await this.coreService
      .expired(this.button)
      .then(e => {
        this.expired = e;
        this.isReadyToClaim();
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getCost() {
    return await this.coreService
      .getCost(this.button)
      .then(cost => {
        this.cost = cost;
      })
      .catch(err => {
        this.onError(err);
      });
  }

  async getBalance() {
    return await this.coreService
      .getBalance(this.account)
      .then(b => {
        this.balance = b;
        this.balanceRefreshing = true;
        console.log(this.balance);
        setTimeout(() => {
          this.balanceRefreshing = false;
        }, 3000);
      })
      .catch(err => {
        this.onError(err);
      });
  }

  @HostListener('window:keydown', ['$event'])
  resetKey(e: KeyboardEvent) {
    if (e.altKey && e.ctrlKey && e.keyCode === 82) {
      this.reset();
    }
  }

  reset() {
    this.waiting = true;
    this.coreService
      .reset(this.button, this.account)
      .then(r => {
        this.refreshGame();
        this.waiting = false;
      })
      .catch(err => {
        this.onError(err);
      });
  }

  onError(err) {
    this.waiting = false;
    console.log(err);
    this.error = err;
    this.coreService.onError(err);
    setTimeout(() => {
      this.error = '';
    }, 7000);
  }

  expandInstructions() {
    if (!this.isMoving) {
      this.instructionsExpanded = !this.instructionsExpanded;
    }
  }

  changeLanguage(i) {
    const langs = Object.getOwnPropertyNames(this.langs);

    localStorage.setItem('lang', langs[i]);
    this.coreService.loadData();
  }
}
