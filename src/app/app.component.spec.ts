import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { CoreService } from './core/core.service';
import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { AppComponent } from './app.component';

declare let require: any;
const buttonABI = require('../assets/abi/Button.json');
const Web3 = require('web3');
import { HttpModule, XHRBackend } from '@angular/http';

describe('AppComponent', () => {
  let service: any;
  let app: any;
  let web3: any;
  const tokenContractAddress = '0xc119606984c41340d791d2b2b59d835733fafe3f';
  const urlTest = 'http://localhost:7545';
  let mainAccount = '';
  let secondaryAccount = '';
  let terciaryAccount = '';

  beforeEach(async(async () => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [HttpClientTestingModule, HttpModule],
      providers: [CoreService, { provide: XHRBackend, useClass: MockBackend }]
    }).compileComponents();

    // const provider = await new Web3(Web3.givenProvider);
    // service = new CoreService(provider, null);

    web3 = new Web3(urlTest);
    service = new CoreService(web3, null);
    service.tokenContractAddress = tokenContractAddress;
    app = new AppComponent(service);
    mainAccount = (await web3.eth.getAccounts())[0];
    secondaryAccount = (await web3.eth.getAccounts())[1];
    terciaryAccount = (await web3.eth.getAccounts())[2];
  }));

  it('should claim the prize', async(() => {
    app
      .setUpGame()
      .then(() => {
        setInterval(() => app.getAge(), 1000);
        setInterval(() => app.isExpired(), 1000);
        setInterval(() => app.getLastParticipant(), 1000);

        app.press();
        const interval = setInterval(() => {
          console.log(app.age);
          console.log(app.action);

          web3.eth
            .sendTransaction({
              to: secondaryAccount,
              from: terciaryAccount,
              value: '1000000000000000'
            })
            .on('transactionHash', hash => {
              // console.log('hash', hash);
            })
            .on('receipt', receipt => {
              // console.log('receipt', receipt);
            })
            .on('confirmation', (confirmationNumber, receipt) => {
              // console.log('confirmationNumber', confirmationNumber);
            })
            .on('error', console.error);
          if (app.action === 'claim') {
            app.claim();
            clearInterval(interval);
            // expect(app.claim).toHaveBeenCalled();
            console.log('claim!');
          }
        }, 5000);
      })
      .catch(err => {
        console.log(err);
      });
  }));

  afterEach(() => {
    service.getAccount().then(account => {
      service.getButton().then(button => {
        service
          .reset(button, account)
          .then(res => {})
          .catch(err => {
            console.log('afterEach error', err);
          });
      });
    });
  });
});
