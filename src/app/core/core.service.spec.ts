import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AppComponent } from './../app.component';
import { TestBed, async, inject } from '@angular/core/testing';
import { CoreService } from './core.service';
import { InjectionToken } from '@angular/core';
import Web3 from 'web3';

declare let require: any;
const buttonABI = require('../../assets/abi/Button.json');
const Web3 = require('web3');

describe('CoreService', () => {
  let service: any;
  let app: any;
  let web3: any;
  const tokenContractAddress = '0xc119606984c41340d791d2b2b59d835733fafe3f';
  const urlTest = 'http://localhost:7545';
  let mainAccount = '';
  let button: any;

  beforeEach(async(async () => {
    web3 = new Web3(urlTest);
    service = new CoreService(web3, null);
    service.tokenContractAddress = tokenContractAddress;
    service.buttonABI = buttonABI;
    app = new AppComponent(service);
    mainAccount = (await web3.eth.getAccounts())[0];
    button = await new web3.eth.Contract(buttonABI, tokenContractAddress);
  }));

  it('should get the contract', (done: DoneFn) => {
    service.getButton().then(b => {
      expect(b).toBeDefined();
      done();
    });
  });

  it('should get the first account in the list', (done: DoneFn) => {
    service.getAccount().then(a => {
      expect(a).toBe(mainAccount);
      done();
    });
  });

  it('should get the get the balance', (done: DoneFn) => {
    service.getBalance(mainAccount).then(balance => {
      expect(balance).toBeGreaterThan(0);
      done();
    });
  });

  it('should get the contract', (done: DoneFn) => {
    service.getButton().then(value => {
      expect(value).toBeDefined();
      done();
    });
  });

  it('should get the cost', (done: DoneFn) => {
    service.getCost(button).then(value => {
      expect(value).toBeDefined();
      done();
    });
  });

  it('should get the expired status', (done: DoneFn) => {
    service.expired(button).then(value => {
      expect(value).toBeDefined();
      done();
    });
  });

  it('should get the waiting time', (done: DoneFn) => {
    service.waitTime(button).then(value => {
      expect(value).toBeDefined();
      done();
    });
  });

  it('should get the last block', (done: DoneFn) => {
    service.lastBlock(button).then(value => {
      expect(value).toBeDefined();
      done();
    });
  });

  // TODO: necesito agregar el tiempo
  it('should press the button, and get the confirmation of the transaction', (done: DoneFn) => {
    service.getCost(button).then(cost => {
      expect(cost).toBeGreaterThan(0);
      const press = service.press(button, mainAccount, cost);
      press
        .on('confirmation', (confirmationNumber, receipt) => {
          expect(confirmationNumber).toBeDefined();
          if (confirmationNumber === 1) {
            done();
          }
        })
        .on('error', error => {
          console.log('press error', error);
          service
            .reset(button, mainAccount)
            .then(res => {})
            .catch(err => {
              console.log('reset error', err);
            });
          done();
        });
    });
  });

  it('should press the button, and claim the reward', (done: DoneFn) => {
    service.getCost(button).then(cost => {
      expect(cost).toBeGreaterThan(0);
      const press = service.press(button, mainAccount, cost);
      press
        .on('confirmation', (confirmationNumber, receipt) => {
          expect(confirmationNumber).toBeDefined();
          if (confirmationNumber === 1) {
            service
              .claim(button, mainAccount)
              .on('confirmation', (c, r) => {
                expect(c).toBeDefined();
                done();
              })
              .on('error', error => {
                console.log('press error', error);
                service
                  .reset(button, mainAccount)
                  .then(res => {})
                  .catch(err => {
                    console.log('reset error', err);
                  });
                done();
              });
          }
        })
        .on('error', error => {
          console.log('press error', error);
          service
            .reset(button, mainAccount)
            .then(res => {})
            .catch(err => {
              console.log('reset error', err);
            });

          done();
        });
    });
  });

  afterEach(() => {
    service
      .reset(button, mainAccount)
      .then(res => {})
      .catch(err => {
        console.log('afterEach error', err);
      });
  });
});
