import { Injectable, InjectionToken, Inject } from '@angular/core';
import Web3 from 'web3';
import { Subject, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Http, Response } from '@angular/http';

declare let require: any;
const buttonABI = require('../../assets/abi/Button.json');

export const WEB3 = new InjectionToken<Web3>('web3', {
  providedIn: 'root',
  factory: () => {
    if (Web3) {
      const resp = new Web3(Web3.givenProvider || 'http://localhost:7545');

      // const resp = new Web3(
      //   new Web3.providers.WebsocketProvider('wss://rinkeby.infura.io/ws')
      // );
      return resp;
    } else {
      console.log('No web3? You should consider trying MetaMask!');
    }
  }
});

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  public locale = window.navigator.language.toLowerCase();
  private account = null;
  private tokenContract: any;
  private tokenContractAddress = '0x693e7a8f8DCc0206A4F9CDAefD0d09e0a510051f';

  public success: Subject<any> = new Subject<any>();
  public error: Subject<any> = new Subject<any>();
  public isDataLoaded: Subject<any> = new Subject<any>();

  constructor(@Inject(WEB3) private web3: any, private http: Http) {
    this.web3 = web3;
    // console.log(this.web3);

    // const subscription = this.web3.eth
    //   .subscribe('newBlockHeaders', (error, blockHeader) => {
    //     if (error) {
    //       return console.error(error);
    //     }

    //     console.log('Successfully subscribed!', blockHeader);
    //   })
    //   .on('data', blockHeader => {
    //     console.log('data: ', blockHeader);
    //   });
  }

  getWeb3() {
    return this.web3;
  }

  public async getBalance(account) {
    return await this.web3.eth.getBalance(account);
  }

  public async getTransactionReceipt(account) {
    return await this.web3.eth.getBalance(account);
  }

  public async getAccount() {
    if (this.account === null) {
      return (await this.web3.eth.getAccounts())[0];
    }
    // return this.web3.eth.getAccounts();
  }

  public async getButton() {
    return await new this.web3.eth.Contract(
      buttonABI,
      this.tokenContractAddress
    );
  }

  public async getCost(button) {
    return await button.methods.COST().call();
  }

  public async expired(button) {
    return await button.methods.expired().call();
  }

  public claim(button, account) {
    return button.methods.claim_treasure().send({ from: account });
  }

  public press(button, account, cost) {
    return button.methods.press_button().send({ from: account, value: cost });
  }

  public async waitTime(button) {
    return await button.methods.WAIT_TIME().call();
  }

  public async lastBlock(button) {
    return await button.methods.lastBlock().call();
  }

  public async reset(button, account) {
    return await button.methods.reset().send({ from: account });
  }
  public async age(button) {
    return await button.methods.age().call();
  }

  public async lastParticipant(button) {
    return await button.methods.lastParticipant().call();
  }

  public onSuccess(success) {
    this.success.next(success);
  }

  public onError(err) {
    this.error.next(err);
  }

  public loadData() {
    this.getMain().subscribe(l => {
      const langCodes = Object.getOwnPropertyNames(l.languages);

      let found = false;
      for (const i in langCodes) {
        if (langCodes[i] === this.locale) {
          found = true;
        }
      }

      // default language
      if (!found) {
        this.locale = 'en-us';
      }

      // if it's not in the localStorage, saves the lang
      if (!localStorage.getItem('lang')) {
        localStorage.setItem('lang', this.locale);
      }

      localStorage.setItem('langs', JSON.stringify(l.languages));

      this.getDataByLanguage().subscribe(data => {
        localStorage.setItem('data', JSON.stringify(data));
        this.isDataLoaded.next(true);
      });
    });
  }

  public getMain() {
    return this.getConfig();
  }

  public getDataByLanguage() {
    const lang = localStorage.getItem('lang');
    return this.getConfig(lang);
  }

  private getConfig(file: string = 'main') {
    const response = this.http.get('./assets/i18n/' + file + '.json');

    return response.pipe(
      map((data: Response) => {
        return this.extractData(data);
      }),
      catchError(
        (err: Response | any): any => {
          // console.log(err);
          return this.handleError(err);
        }
      )
    );
  }

  private extractData(data: Response) {
    if (data['_body'] !== '') {
      const body = data.json() ? data.json()['data'] : data;
      return body || {};
    } else {
      return {};
    }
  }

  handleError(error: Response) {
    console.log(error);
    return throwError(error || 'Internal server error');
  }
}
