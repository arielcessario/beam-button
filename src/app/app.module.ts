import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppComponent } from './app.component';
import { CoreService } from './core/core.service';
import { DraggableDirective } from './shared/draggable.directive';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [AppComponent, DraggableDirective],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot()
  ],
  providers: [CoreService],
  bootstrap: [AppComponent]
})
export class AppModule {}
