# Technical details
The front end is designed with Angular 6, using an standar architecture. Also uses web3.js, beta 33.  

# Run
npm install -g @angular/cli
Download from repo (git clone git@bitbucket.org:arielcessario/beam-button.git or git clone https://arielcessario@bitbucket.org/arielcessario/beam-button.git)
cd beam-button
npm install
ng serve --open

# Test
ng test

# Reseting the game
press alt + ctrl + R

# BeamButton

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

